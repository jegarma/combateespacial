from CombateEspacial.models import Jugador
from CombateEspacial.models import RegistroPartida
from CombateEspacial.models import Sala
from django.contrib import admin

admin.site.register(Jugador)
admin.site.register(RegistroPartida)
admin.site.register(Sala)
